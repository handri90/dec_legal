<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<title>E-Regulation - Kotawaringin Barat</title>
	<meta content="" name="description">
	<meta content="" name="keywords">

	<!-- Favicons -->

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

	<!-- Vendor CSS Files -->
	<link href="<?php echo base_url(); ?>assets/template_login/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/template_login/vendor/icofont/icofont.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/template_login/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/template_login/vendor/remixicon/remixicon.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/template_login/vendor/venobox/venobox.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/template_login/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/template_login/vendor/aos/aos.css" rel="stylesheet">
	<link href="<?php echo base_url(); ?>assets/css/HoldOn.min.css" rel="stylesheet" />

	<!-- Template Main CSS File -->
	<link href="<?php echo base_url(); ?>assets/template_login/css/style.css" rel="stylesheet">

	<script>
		var base_url = "<?php echo base_url(); ?>";
	</script>

	<!-- =======================================================
  * Template Name: Arsha - v2.3.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

	<!-- ======= Header ======= -->
	<header id="header" class="fixed-top ">
		<div class="container d-flex align-items-center">

			<h1 class="logo mr-auto"><a href="index.html">E-Regulation</a></h1>

		</div>
	</header><!-- End Header -->

	<!-- ======= Hero Section ======= -->
	<section id="hero" class="d-flex align-items-center">

		<div class="container">
			<div class="row">
				<div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
					<h1>E-Regulation</h1>
					<h2>Aplikasi Penyusunan Keputusan Bupati<br />Kabupaten Kotawaringin Barat</h2>
					<div class="d-lg-flex">
						<a href="#myModal" data-toggle="modal" class="btn-get-started scrollto">Login</a>
					</div>
				</div>
				<div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
					<img src="<?php echo base_url(); ?>assets/template_login/img/login.png" class="img-fluid" alt="">
				</div>
			</div>
		</div>

		<div id="myModal" class="modal fade">
			<div class="modal-dialog modal-login">
				<div class="modal-content">
					<div class="modal-header">
						<h4 class="modal-title">Login</h4>
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					</div>
					<div class="modal-body">
						<span class='error_field'></span>
						<div class="form-group">
							<label>Username</label>
							<input type="text" class="form-control" required="required" name="username">
						</div>
						<div class="form-group">
							<div class="clearfix">
								<label>Password</label>
							</div>

							<input type="password" class="form-control" required="required" name="password">
						</div>
					</div>
					<div class="modal-footer justify-content-between">
						<input type="submit" class="btn btn-primary" value="Login" onclick="check_auth()">
					</div>
				</div>
			</div>
		</div>

	</section><!-- End Hero -->

	<!-- Vendor JS Files -->
	<script src="<?php echo base_url(); ?>assets/template_login/vendor/jquery/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_login/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_login/vendor/jquery.easing/jquery.easing.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_login/vendor/php-email-form/validate.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_login/vendor/waypoints/jquery.waypoints.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_login/vendor/isotope-layout/isotope.pkgd.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_login/vendor/venobox/venobox.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_login/vendor/owl.carousel/owl.carousel.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/template_login/vendor/aos/aos.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/HoldOn.min.js"></script>

	<!-- Template Main JS File -->
	<script src="<?php echo base_url(); ?>assets/template_login/js/main.js"></script>

	<script>
		let optionsHoldOn = {
			theme: "sk-cube-grid",
			message: 'Loading',
			textColor: "white"
		};

		function check_auth() {
			let username = $("input[name='username']").val();
			let password = $("input[name='password']").val();
			$(".error_field").html("");

			if (!username) {
				$(".error_field").html("<div class='alert alert-danger'>Field username tidak boleh kosong</div>");
			} else if (!password) {
				$(".error_field").html("<div class='alert alert-danger'>Field Password tidak boleh kosong</div>");
			} else {
				$.ajax({
					url: base_url + 'login/act_login',
					data: {
						username: username,
						password: password
					},
					type: 'POST',
					beforeSend: function() {
						HoldOn.open(optionsHoldOn);
					},
					success: function(response) {
						if (response == true) {
							$(".error_field").html("<div class='alert alert-success'>Berhasil...</div>");
							location.reload();
						} else {
							$(".error_field").html("<div class='alert alert-danger'>Username dan Password salah</div>");
						}
					},
					complete: function() {
						HoldOn.close();
					}
				});
			}
		}

		$(function() {

			$('#myModal').on('keypress', function(event) {
				var keycode = (event.keyCode ? event.keyCode : event.which);
				if (keycode == '13') {
					check_auth();
				}
			});
		});
	</script>

</body>

</html>