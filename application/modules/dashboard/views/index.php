<div class="main-content">
    <section class="section">
        <?php echo $breadcrumb_main; ?>
        <div class="section-body">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="empty-state" data-height="600">
                                <img height="300px" src="<?php echo base_url(); ?>assets/img/drawkit/drawkit-full-stack-man-colour.svg" alt="image">
                                <h2 class="mt-0">Dashboard Aplikasi Penyusunan Keputusan Bupati</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>